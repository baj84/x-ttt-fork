import React from 'react';

const SetName = props => {
  const [name, setName] = React.useState('');

  return (
    <div id="SetName">
      <h1>Set Name</h1>
      <form
        onSubmit={e => {
          e.preventDefault();

          props.onSetName(name.trim());
        }}
      >
        <div className="input_holder left">
          <label>Name </label>
          <input
            type="text"
            className="input name"
            placeholder="Name"
            onChange={e => setName(e.target.value)}
          />
        </div>

        <button type="submit" className="button">
          <span>
            SAVE <span className="fa fa-caret-right"></span>
          </span>
        </button>
      </form>
    </div>
  );
};

export default SetName;
