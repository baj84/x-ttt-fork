import React, { Component } from 'react';

import io from 'socket.io-client';

import TweenMax from 'gsap';

import rand_arr_elem from '../../helpers/rand_arr_elem';
import rand_to_fro from '../../helpers/rand_to_fro';

const ENALBE_SMART_COMPUTER = true;

export default class GameMain extends Component {
  constructor(props) {
    super(props);

    this.smartComputerMove = this.smartComputerMove.bind(this);
    this.randomComputerMove = this.randomComputerMove.bind(this);
    this.resetGame = this.resetGame.bind(this);

    this.win_sets = [
      ['c1', 'c2', 'c3'],
      ['c4', 'c5', 'c6'],
      ['c7', 'c8', 'c9'],

      ['c1', 'c4', 'c7'],
      ['c2', 'c5', 'c8'],
      ['c3', 'c6', 'c9'],

      ['c1', 'c5', 'c9'],
      ['c3', 'c5', 'c7'],
    ];

    if (this.props.game_type != 'live')
      this.state = {
        cell_vals: {},
        next_turn_ply: true,
        game_play: true,
        game_stat: 'Start game',
      };
    else {
      this.sock_start();

      this.state = {
        cell_vals: {},
        next_turn_ply: true,
        game_play: false,
        game_stat: 'Connecting',
      };
    }
  }

  resetGame() {
    this.state = {
      cell_vals: {},
      next_turn_ply: true,
      game_play: true,
      game_stat: 'Start game',
    };

    this.props.onEndGame();
  }
  //	------------------------	------------------------	------------------------

  componentDidMount() {
    TweenMax.from('#game_stat', 1, {
      display: 'none',
      opacity: 0,
      scaleX: 0,
      scaleY: 0,
      ease: Power4.easeIn,
    });
    TweenMax.from('#game_board', 1, {
      display: 'none',
      opacity: 0,
      x: -200,
      y: -200,
      scaleX: 0,
      scaleY: 0,
      ease: Power4.easeIn,
    });
  }

  //	------------------------	------------------------	------------------------
  //	------------------------	------------------------	------------------------

  sock_start() {
    this.socket = io(app.settings.ws_conf.loc.SOCKET__io.u);

    this.socket.on(
      'connect',
      function (data) {
        // console.log('socket connected', data)

        this.socket.emit('new player', { name: app.settings.curr_user.name });
      }.bind(this)
    );

    this.socket.on(
      'pair_players',
      function (data) {
        // console.log('paired with ', data)

        this.setState({
          next_turn_ply: data.mode == 'm',
          game_play: true,
          game_stat: 'Playing with ' + data.opp.name,
        });
      }.bind(this)
    );

    this.socket.on('opp_turn', this.turn_opp_live.bind(this));

    this.socket.on('opponent_quit', function () {
      alert('Your opponent has quit!');
    });
  }

  //	------------------------	------------------------	------------------------
  //	------------------------	------------------------	------------------------

  componentWillUnmount() {
    this.socket && this.socket.disconnect();
  }

  //	------------------------	------------------------	------------------------

  getMarkerElements(marker) {
    let item = {
      x: <i className="fa fa-times fa-5x"></i>,
      o: <i className="fa fa-circle-o fa-5x"></i>,
    };

    if (marker === 'dogscats') {
      item.x = (
        <div className="marker-dog">
          <img src="/images/dog.png" style={{ width: 80, height: 'auto' }} />
        </div>
      );
      item.o = (
        <div className="marker-cat">
          <img src="/images/cat.png" style={{ width: 80, height: 'auto' }} />
        </div>
      );
    }

    return item;
  }

  cell_cont(c) {
    const { cell_vals } = this.state;

    const marker = this.getMarkerElements(this.props.marker);

    return (
      <div>
        {cell_vals && cell_vals[c] == 'x' && marker.x}
        {cell_vals && cell_vals[c] == 'o' && marker.o}
      </div>
    );
  }

  //	------------------------	------------------------	------------------------

  render() {
    const { cell_vals } = this.state;

    return (
      <div id="GameMain">
        <h1>Play {this.props.game_type}</h1>

        <div id="game_stat">
          <div id="game_stat_msg">{this.state.game_stat}</div>
          {this.state.game_play && (
            <div id="game_turn_msg">
              {this.state.next_turn_ply ? 'Your turn' : 'Opponent turn'}
            </div>
          )}

          {!this.state.game_play && (
            <a
              onClick={() => {
                this.resetGame();
              }}
              className="play-again"
            >
              Play Again
            </a>
          )}
        </div>

        <div id="game_board">
          <table>
            <tbody>
              <tr>
                <td
                  id="game_board-c1"
                  ref="c1"
                  onClick={this.click_cell.bind(this)}
                >
                  {' '}
                  {this.cell_cont('c1')}{' '}
                </td>
                <td
                  id="game_board-c2"
                  ref="c2"
                  onClick={this.click_cell.bind(this)}
                  className="vbrd"
                >
                  {' '}
                  {this.cell_cont('c2')}{' '}
                </td>
                <td
                  id="game_board-c3"
                  ref="c3"
                  onClick={this.click_cell.bind(this)}
                >
                  {' '}
                  {this.cell_cont('c3')}{' '}
                </td>
              </tr>
              <tr>
                <td
                  id="game_board-c4"
                  ref="c4"
                  onClick={this.click_cell.bind(this)}
                  className="hbrd"
                >
                  {' '}
                  {this.cell_cont('c4')}{' '}
                </td>
                <td
                  id="game_board-c5"
                  ref="c5"
                  onClick={this.click_cell.bind(this)}
                  className="vbrd hbrd"
                >
                  {' '}
                  {this.cell_cont('c5')}{' '}
                </td>
                <td
                  id="game_board-c6"
                  ref="c6"
                  onClick={this.click_cell.bind(this)}
                  className="hbrd"
                >
                  {' '}
                  {this.cell_cont('c6')}{' '}
                </td>
              </tr>
              <tr>
                <td
                  id="game_board-c7"
                  ref="c7"
                  onClick={this.click_cell.bind(this)}
                >
                  {' '}
                  {this.cell_cont('c7')}{' '}
                </td>
                <td
                  id="game_board-c8"
                  ref="c8"
                  onClick={this.click_cell.bind(this)}
                  className="vbrd"
                >
                  {' '}
                  {this.cell_cont('c8')}{' '}
                </td>
                <td
                  id="game_board-c9"
                  ref="c9"
                  onClick={this.click_cell.bind(this)}
                >
                  {' '}
                  {this.cell_cont('c9')}{' '}
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <button
          type="submit"
          onClick={this.end_game.bind(this)}
          className="button"
        >
          <span>
            End Game <span className="fa fa-caret-right"></span>
          </span>
        </button>
      </div>
    );
  }

  //	------------------------	------------------------	------------------------
  //	------------------------	------------------------	------------------------

  click_cell(e) {
    // console.log(e.currentTarget.id.substr(11))
    // console.log(e.currentTarget)

    if (!this.state.next_turn_ply || !this.state.game_play) return;

    const cell_id = e.currentTarget.id.substr(11);
    if (this.state.cell_vals[cell_id]) return;

    if (this.props.game_type != 'live') this.turn_ply_comp(cell_id);
    else this.turn_ply_live(cell_id);
  }

  //	------------------------	------------------------	------------------------
  //	------------------------	------------------------	------------------------

  turn_ply_comp(cell_id) {
    let { cell_vals } = this.state;

    cell_vals[cell_id] = 'x';

    TweenMax.from(this.refs[cell_id], 0.7, {
      opacity: 0,
      scaleX: 0,
      scaleY: 0,
      ease: Power4.easeOut,
    });

    // this.setState({
    // 	cell_vals: cell_vals,
    // 	next_turn_ply: false
    // })

    // setTimeout(this.turn_comp.bind(this), rand_to_fro(500, 1000));

    this.state.cell_vals = cell_vals;

    this.check_turn();
  }

  //	------------------------	------------------------	------------------------

  /**
   * Checks if the opponent is about to win and if so returns the next
   * move the player should make.
   * @param {array} winSets
   * @param {array} playerMoves
   * @param {array} opponentMoves
   * @returns integer
   */
  getPotentialWinIndex(winSets, playerMoves, opponentMoves) {
    let nextMove = -1;

    let potentialWins = winSets.filter(
      array =>
        array.filter(item => {
          return opponentMoves.indexOf(item) > -1;
        }).length === 2
    );

    if (potentialWins.length > 0) {
      potentialWins.filter(array =>
        //get the index of the next computer move
        array.filter(item => {
          if (
            potentialWins.indexOf(item) === -1 &&
            opponentMoves.indexOf(item) === -1 &&
            playerMoves.indexOf(item) === -1
          ) {
            nextMove = item;
          }
        })
      );
    }

    return nextMove;
  }

  /**
   * Makes a smart move for the computer
   */
  smartComputerMove() {
    let { cell_vals } = this.state;
    let centerCorners = [0, 2, 4, 6, 8];

    // Restructure the winning sets to index based arrays
    const winSets = this.win_sets.map(set => {
      return set.map(item => parseInt(item.replace('c', '')) - 1);
    });

    let nextMove = -1;

    // cell data
    // 2 = computers move, 0 = opponent move, 1 = available
    let cells = new Array(9);
    let opponentMoves = [];
    let computerMoves = [];

    // Restructure the cell data to be a flat index
    // and split up the taken cells into computers and opponents
    for (let i = 0; i < cells.length; i++) {
      let cellVal = cell_vals['c' + (i + 1)];

      // All cells on by default
      cells[i] = 1;

      // Cell is already taken by computer
      if (cellVal === 'o') {
        computerMoves.push(i);
        cells[i] = 2;
      }

      // Cell is taken by opponent so turn it off
      if (cellVal === 'x') {
        cells[i] = 0;
        opponentMoves.push(i);

        // Remove opponent choice from the centerAndCorners index
        const index = centerCorners.indexOf(i);
        if (index > -1) {
          centerCorners.splice(index, 1);
        }
      }
    }

    // If it's the computers first go choose the middle or a corner
    // position as it's an advantage
    if (computerMoves.length === 0) {
      nextMove =
        centerCorners[Math.floor(Math.random() * centerCorners.length)];
    } else {
      // First check to make sure the opponent isn't about to win.
      nextMove = this.getPotentialWinIndex(
        winSets,
        computerMoves,
        opponentMoves
      );

      // Opponent isn't about to win so let's find the best next
      // move for the computer
      if (nextMove === -1) {
        nextMove = this.getPotentialWinIndex(
          winSets,
          opponentMoves,
          computerMoves
        );
      }
    }

    if (nextMove === -1) {
      // It's impossible to win. Play a random move.
      this.randomComputerMove();
    } else {
      cell_vals[`c${nextMove + 1}`] = 'o';

      TweenMax.from(this.refs[`c${nextMove + 1}`], 0.7, {
        opacity: 0,
        scaleX: 0,
        scaleY: 0,
        ease: Power4.easeOut,
      });

      this.state.cell_vals = cell_vals;

      this.check_turn();
    }
  }

  /*
   * Makes a random choice for the computer
   */
  randomComputerMove() {
    let { cell_vals } = this.state;
    let empty_cells_arr = [];

    for (let i = 1; i <= 9; i++)
      !cell_vals['c' + i] && empty_cells_arr.push('c' + i);

    const c = rand_arr_elem(empty_cells_arr);
    cell_vals[c] = 'o';

    TweenMax.from(this.refs[c], 0.7, {
      opacity: 0,
      scaleX: 0,
      scaleY: 0,
      ease: Power4.easeOut,
    });

    this.state.cell_vals = cell_vals;

    this.check_turn();
  }

  turn_comp() {
    if (ENALBE_SMART_COMPUTER) {
      this.smartComputerMove();
    } else {
      this.randomComputerMove();
    }
  }

  //	------------------------	------------------------	------------------------
  //	------------------------	------------------------	------------------------

  turn_ply_live(cell_id) {
    let { cell_vals } = this.state;

    cell_vals[cell_id] = 'x';

    TweenMax.from(this.refs[cell_id], 0.7, {
      opacity: 0,
      scaleX: 0,
      scaleY: 0,
      ease: Power4.easeOut,
    });

    this.socket.emit('ply_turn', { cell_id: cell_id });

    // this.setState({
    // 	cell_vals: cell_vals,
    // 	next_turn_ply: false
    // })

    // setTimeout(this.turn_comp.bind(this), rand_to_fro(500, 1000));

    this.state.cell_vals = cell_vals;

    this.check_turn();
  }

  //	------------------------	------------------------	------------------------

  turn_opp_live(data) {
    let { cell_vals } = this.state;
    let empty_cells_arr = [];

    const c = data.cell_id;
    cell_vals[c] = 'o';

    TweenMax.from(this.refs[c], 0.7, {
      opacity: 0,
      scaleX: 0,
      scaleY: 0,
      ease: Power4.easeOut,
    });

    // this.setState({
    // 	cell_vals: cell_vals,
    // 	next_turn_ply: true
    // })

    this.state.cell_vals = cell_vals;

    this.check_turn();
  }

  //	------------------------	------------------------	------------------------
  //	------------------------	------------------------	------------------------
  //	------------------------	------------------------	------------------------

  check_turn() {
    const { cell_vals } = this.state;

    let win = false;
    let set;
    let fin = true;

    if (this.props.game_type != 'live') this.state.game_stat = 'Play';

    for (let i = 0; !win && i < this.win_sets.length; i++) {
      set = this.win_sets[i];
      if (
        cell_vals[set[0]] &&
        cell_vals[set[0]] == cell_vals[set[1]] &&
        cell_vals[set[0]] == cell_vals[set[2]]
      )
        win = true;
    }

    for (let i = 1; i <= 9; i++) !cell_vals['c' + i] && (fin = false);

    // win && console.log('win set: ', set)

    if (win) {
      this.refs[set[0]].classList.add('win');
      this.refs[set[1]].classList.add('win');
      this.refs[set[2]].classList.add('win');

      TweenMax.killAll(true);
      TweenMax.from('td.win', 1, { opacity: 0, ease: Linear.easeIn });

      this.setState({
        game_stat: (cell_vals[set[0]] == 'x' ? 'You' : 'Opponent') + ' win',
        game_play: false,
      });

      this.socket && this.socket.disconnect();
    } else if (fin) {
      this.setState({
        game_stat: 'Draw',
        game_play: false,
      });

      this.socket && this.socket.disconnect();
    } else {
      this.props.game_type != 'live' &&
        this.state.next_turn_ply &&
        setTimeout(this.turn_comp.bind(this), rand_to_fro(500, 1000));

      this.setState({
        next_turn_ply: !this.state.next_turn_ply,
      });
    }
  }

  //	------------------------	------------------------	------------------------

  end_game() {
    this.socket && this.socket.disconnect();

    this.props.onEndGame();
  }
}
