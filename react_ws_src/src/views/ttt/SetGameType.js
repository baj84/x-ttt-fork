import React, { Component } from 'react';

export default class SetGameType extends Component {
  constructor(props) {
    super(props);

    this.state = {
      marker: 'dogscats',
    };
  }

  //	------------------------	------------------------	------------------------

  render() {
    return (
      <div id="SetGameType">
        <h1>Choose game type</h1>
        <button
          type="submit"
          onClick={this.selTypeLive.bind(this)}
          className="button long"
        >
          <span>
            Live against another player{' '}
            <span className="fa fa-caret-right"></span>
          </span>
        </button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <button
          type="submit"
          onClick={this.selTypeComp.bind(this)}
          className="button long"
        >
          <span>
            Against a computer <span className="fa fa-caret-right"></span>
          </span>
        </button>
        <div className="marker-type">
          <h3>Choose Marker Type</h3>
          <div>
            <input
              type="radio"
              name="marker"
              value="dogscats"
              checked={this.state.marker === 'dogscats'}
              onClick={() => {
                this.setState({ marker: 'dogscats' });

                this.props.onSetMarker('dogscats');
              }}
              id="dogscats-radio"
            />{' '}
            <label for="dogscats-radio">Dogs &amp; Cats</label>
          </div>
          <div>
            <input
              type="radio"
              name="marker"
              value="naughtscrosses"
              checked={this.state.marker === 'naughtscrosses'}
              id="naughtscrosses-radio"
              onClick={() => {
                this.setState({ marker: 'naughtscrosses' });
                this.props.onSetMarker('naughtscrosses');
              }}
            />{' '}
            <label for="naughtscrosses-radio">Naughts &amp; Crosses</label>
          </div>
        </div>
      </div>
    );
  }

  //	------------------------	------------------------	------------------------

  selTypeLive(e) {
    // const { name } = this.refs
    // const { onSetType } = this.props
    // onSetType(name.value.trim())

    this.props.onSetType('live');
  }

  //	------------------------	------------------------	------------------------

  selTypeComp(e) {
    // const { name } = this.refs
    // const { onSetType } = this.props
    // onSetType(name.value.trim())

    this.props.onSetType('comp');
  }
}
