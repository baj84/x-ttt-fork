# A simple example of a full website built with React.js stack

## React development source and testing

Major libraries used:
- react
- webpack
- babel
- react-router
- ampersand
- sass
- jest

---

##For demonstration purposes only.

---
---
## Requirements

Node v7
~~~bash
nvm use v7.0.0
~~~

xcode-select
To install and enable CLI on a mac you can run this:
~~~bash
xcode-select --install; sudo xcode-select --switch /Library/Developer/CommandLineTools
~~~

## Development
The core of the functionality is in `./src` folder. To develop and make changes to it you can test locally by running the devServer: `npm start`.

To test the full websocket functionality on your localhost you need to build the project `npm run build`.
This will bundle the src and copy it to the `../WS/public` folder. You can then run the WS app to test it. See the [../WS/README.md](/WS/README.md).

The main configuration can be found in `./static/ws_conf.xml`.

## Install

~~~bash
npm install
~~~

To start the development web server use the following command and turn your browser to [http://localhost:3000](http://localhost:3000) or [http://0.0.0.0:3000](http://0.0.0.0:3000) 

~~~bash
npm start
~~~

To run code lint

~~~bash
npm run lint
~~~

To run code tests

~~~bash
npm run test
~~~

To build the site 

~~~bash
npm run build
~~~
