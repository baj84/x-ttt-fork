# Web-Socket Backend for TickTacToe Game

## Architecture
- Node / Express
- socket.io

## Requirements

Node v7
~~~bash
nvm use v7.0.0
~~~

## Install

~~~bash
npm install
~~~

~~~bash
npm start
~~~

## Build Process
You must build the src of the project from the `./react_ws_src`, which will bundle the source and copy it to `./WS/public`