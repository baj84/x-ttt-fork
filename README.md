# A simple example of a full multiplayer game web app built with React.js and Node.js stack

## Improvements made in this Fork
 - Improve documentation on how to build and set up the app
 - Fixed local issue with images not working
 - Added prettier and re-formatted files to improve readability
 - Fixed bug: build:webpack_dev was using prod config
 - Upgraded: React v16, React-dom, React-router, PropTypes
 - Add Babel plugin so we can use the spread operator
 - Refactored SetName.js to a functional component and to use ReactHooks.
 - UI improvements:
	- Changed colour of winning to green instead of red
	- Wrapped forms so you can press 'Enter' to submit (when you enter your name)
	- Made it so you can choose your marker types (dogs&cats)

 - Made the computer make a smarter move instead of just random. See smartComputerMove()
 - Notify the player when their opponent quits (added new socket.io event)
 - Added a 'Play again' link
 - Added 'build:copyall_mac' to copy build over to WS on mac
 - Fixed Jest tests

## Other improvements I'd make with more time
 - Make all components functional components and use react hooks
 - Remove all refs
 - Add scoring
 - Refactor the game code to into separate models such as Board and Player.
 - Simplify the Board by using a flat index based array, no need for object.
 - Don't hardcode the size of the grid, so it can be a 6x6 board
 - Improve build process so manual copy commands aren't required.
 - Write unit tests for my smart move code
 - Remove builds from git repo

Major libraries used on front end:
- react
- webpack
- babel
- react-router
- ampersand
- sass
- jest

Major libraries used on server:
- node.js
- socket.io
- express

### Folder structure:
- **WS** - server side and compiled front end
- **react_ws_src** - React development source and testing

---

### View it online at 
https://x-ttt.herokuapp.com/

#### Configurable with external XML file - 
https://x-ttt.herokuapp.com/ws_conf.xml

---

##For demonstration purposes only.
